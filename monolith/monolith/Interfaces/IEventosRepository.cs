﻿using monolith.Models;

namespace monolith.Interfaces
{
    public interface IEventosRepository
    {
        bool put(Eventos evento);
    }
}