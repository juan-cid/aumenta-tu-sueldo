﻿using System.Threading.Tasks;
using monolith.Models;

namespace monolith.Interfaces
{
    public interface IAtsRepository
    {
        IndicadoresPrev GetbySueldo(double sueldo);
        IndicadoresPrev GetbyId(int id);
        IndicadoresParAFP GetComisionAfp(int afp);
        RentasTopes GetSueldoTopeImponibles();
        IndicadoresPrev ObtenerPorNivel(int nivel, int mes, int año);
        Parametros leerParametros(string parametro);
        Task<ComisionesAFP> ObtenerComisiones();
    }
}