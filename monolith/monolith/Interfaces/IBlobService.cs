﻿using System.IO;
using System.Threading.Tasks;

namespace monolith.Interfaces
{
    public interface IBlobService
    {
        Task<string> SubirArchivo(Stream archivoStream,string nombreArchivo);
    }
}