﻿using System.Threading.Tasks;
using monolith.Models;

namespace monolith.Interfaces
{
    public interface IRentaBrutaRepository
    {
        Task<ResultadoAts> ObtenerRentaBruta(double RentaLiquida, int Afp);
    }
}