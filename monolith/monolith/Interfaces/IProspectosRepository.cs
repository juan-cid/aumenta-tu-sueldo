﻿using System.Collections.Generic;
using monolith.Models;

namespace monolith.Interfaces
{
    public interface IProspectosRepository
    {
        bool put(Prospectos prospecto);
        List<Prospectos> GetAllxFecha(string fecha);

        Prospectos ObtenerPorId(int id);


    }
}