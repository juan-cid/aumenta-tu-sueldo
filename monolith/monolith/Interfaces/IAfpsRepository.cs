﻿using System.Data;
using System.Threading.Tasks;
using monolith.Models;

namespace monolith.Interfaces
{
    public interface IAfpsRepository
    {
       Task<Afps> GetbyId(int id);
    }
}