﻿using System;
using System.Threading.Tasks;
using monolith.Models;

namespace monolith.Interfaces
{
    public interface ISimuladorRepository
    {
        bool put(Simulador simulador);

        Task<bool> GuardarEnvioCorreo();

        EnvioSimulacionCorreo ObtenerPorFecha(DateTime dia);
    }
}