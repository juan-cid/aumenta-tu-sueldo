﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace monolith.Interfaces
{
    public interface ISendgridServiceRepositorio
    {
        public Task<string> EnviarCorreoText(string mail_remitente, string remitente, string mail_receptor,
            string nombre, string asunto, string texto);

        public Task<string> EnviarCorreoHTML(string mail_remitente, string remitente, string mail_receptor,
            string nombre, string asunto, string textoplano, string html);
        
        
        public Task<string> EnviarCorreoAttachment(string mail_remitente, string remitente, string mail_receptor,
            string nombre, string asunto, string textoplano, string html,FileContentResult archivo);
        
    }
}