﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace monolith.Models
{
    public class Afps
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CodigoSIAFP { get; set; }
    }
}
