﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace monolith.Models
{
    public class Prospectos
    {

        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        
        public string Fono_Movil { get; set; }
        
        [JsonProperty("afpOrigen")]
        public string Afp_Origen { get; set; }
        
        [JsonProperty("codAfpOrigen")]
        public string Cod_Afp_Origen { get; set; }
        
        public string Fec_Ing_reg { get; set; }
        public string Canal { get; set; }
        public string Horario { get; set; }
        public float Sueldo { get; set; }
        public double Resultado { get; set; }
    }
}
