﻿namespace monolith.Models
{
    public class ComisionesAFP
    {
        //0=Capital
        //1=Cuprum
        //2=HAbitat
        //3=Planvital
        //4=Provida
        //5=Modelo
        //6=Uno
        public string mes { get; set; }
        public int anio { get; set; }
        public string capital { get; set; }
        public string cuprum { get; set; }
        public string habitat { get; set; }
        public string modelo { get; set; }
        public string planvital { get; set; }
        public string provida { get; set; }
        public string uno { get; set; }
    }
}