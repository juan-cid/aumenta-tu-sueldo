﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace monolith.Models
{
    public class Eventos
    {
        public Eventos()
        {
            
        }

        public Eventos(string sesionId, int eventoId, int result0, int result1, int result2, string rut, DateTime horaEvento)
        {
            Sesion_id = sesionId;
            Evento_id = eventoId;
            Result_0 = result0;
            Result_1 = result1;
            Result_2 = result2;
            Rut = rut;
            HoraEvento = horaEvento == new DateTime() ? DateTime.UtcNow : horaEvento;
        }
        public string Sesion_id { get; set; }
        public int Evento_id { get; set; }
        public int  Result_0 { get; set; }
        public int Result_1 { get; set; }
        public int Result_2 { get; set; }
        public string Rut { get; set; }
        
        public DateTime HoraEvento { get; set; }

    }
}
