﻿namespace monolith.Models
{
    public class IndicadorComision
    {
        public int Id { get; set; }
        public string Afp { get; set; }
        public double? TasaAfp { get; set; }
        public double ComisionAfp { get; set; }
        public double? Sis { get; set; }
        public double TasaAfpInd { get; set; }
        public double ComisionAfpInd { get; set; }
        public int? Mes { get; set; }
        public int? Año { get; set; }
        public double? UfMes { get; set; }
        public double? UfMesAnt { get; set; }
        public double? UtmMes { get; set; }
        public double? Rti { get; set; }
        public double? RtisegSe { get; set; }
        public double? Vrtiporc { get; set; }
        public double? VrtisegSePorc { get; set; }
        public double? Vrtipeso { get; set; }
        public double? VrtisegSePeso { get; set; }
        public double? Afcporc { get; set; }
        public double? ApvtopeMensUf { get; set; }
        public double? ApvtopeAnualUf { get; set; }
        public double? ApvtopeMensPeso { get; set; }
        public double? ApvtopeAnualPeso { get; set; }
        public int Afp_Id { get; set; }
    }
}