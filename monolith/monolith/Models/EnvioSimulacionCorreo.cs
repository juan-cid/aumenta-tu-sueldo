﻿using System;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace monolith.Models
{
    public class EnvioSimulacionCorreo
    {
        public int id { get; }
        public DateTime FechaEnvio { get; set; }
        public bool Enviado { get; set; }
    }
}