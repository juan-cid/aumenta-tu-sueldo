﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace monolith.Models
{
    public class IndicadoresParAFP
    {
        public string Afp { get; set; }
        public int? Afp_Id { get; set; }
        public float TasaAfp { get; set; }
        public float ComisionAfp { get; set; }
        public double? UtmMes { get; set; }
        public double? UfMes { get; set; }

    }
}
