﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace monolith.Models
{
    public class Simulador
    {

        public string Nombre { get; set; }
        public string Rut { get; set; }
        public string Correo { get; set; }

        public string Telefono_movil { get; set; }
        public float Sueldo { get; set; }
        public string Cod_Afp_Actual { get; set; }
        public DateTime fecha_simulacion { get; set; }

        public double resultado  { get; set; }
       
    }
}
