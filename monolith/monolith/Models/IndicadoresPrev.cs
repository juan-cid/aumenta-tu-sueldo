﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace monolith.Models
{
    public class IndicadoresPrev
    {

        public int Id { get; set; }
        public double RentaLiquidaDesde { get; set; }
        public double RentaLiquidaHasta { get; set; }
        public double CantidadRebajar { get; set; }
        public double Impuesto { get; set; }
        public int Mes { get; set; }
        public int Año { get; set; }
        public int Nivel { get; set; }
    }
}
