import React from "react";
import "../assets/styles/Calculadora.css";
import ahorro from "../assets/svg/ahorro.svg";
import pareja from "../assets/svg/couple.svg";
import { Helmet } from "react-helmet";
import FormCalculadora from "../components/FormCalculadora";

export default function Calculadora() {

    localStorage.setItem("sueldoAnual", 0);

    return (
        <div>
            <Helmet>
                <title>Aumenta tu Sueldo | Pagar Menos Comisión en AFP | AFP Modelo</title>
                <meta name="description" content="Aumenta tu sueldo líquido, pagando una menor comisión de AFP. Simula tu aumento de sueldo al cambiarte a AFP Modelo." />
                <meta name="robots" content="index, follow" />
            </Helmet>
            <div className="background">
                <section>
                    <div className="row w-100 container-padre calculadora">
                        <div className="col-12 d-flex justify-content-center mobile">
                            <div className="container-title">
                                <h1>Conoce cuánto</h1>
                                <h1>aumentaría tu sueldo,</h1>
                                <h1>al cambiarte a</h1>
                                <h1>AFP Modelo.</h1>
                                <p>
                                    Únete a la AFP con una de las comisiones más bajas del mercado.
                                </p>
                                <br></br>
                                <br></br>
                                <img src={ahorro} alt="cerditoAhorro" />
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-5 bloque desktop">
                            <div className="txtDesktop">
                                <div className="container-title">
                                    <h1>Conoce cuánto</h1>
                                    <h1>aumentaría tu</h1>
                                    <h1>sueldo,</h1>
                                    <h1 className="green">al cambiarte a</h1>
                                    <h1 className="green">AFP Modelo.</h1>
                                    <p>
                                        Únete a la AFP con una de las comisiones más bajas del
                                        mercado.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-2 bloque desktop medio">
                            <img src={pareja} alt="pareja modelo" />
                        </div>
                        <div className="col-sm-12  col-md-5 bloque form">
                            <div className="card formulario">
                                <div className="card-body">
                                    <FormCalculadora />
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}
