import React, { useState, useEffect } from "react";
import mujer from "../assets/svg/resultadoMina.svg";
import check from "../assets/svg/iconCheck.svg";
import rentabilidad from "../assets/svg/iconFinanzas.svg";
import comisiones from "../assets/svg/iconComisiones.svg";
import sac from "../assets/svg/iconServicio.svg";
import aumentarSueldo from "../assets/svg/mujerCerdito.svg";
import flecha from "../assets/svg/arrow-down.svg";
import mujerSAC from "../assets/svg/mujerSAC.svg";
import "../assets/styles/Resultado.css";
import "../utils/validations";
import { sueldoFormateador } from "../utils/validations";
import axios from "axios";
import { Helmet } from "react-helmet";

export default function Resultado() {
    const [sueldo, setSueldo] = useState("");
    const [text, setText] = useState("");
    const headers = {
        "Content-Type": "application/json"
    };

    let body_eventos = {
        sesion_id: localStorage.getItem("sessionId"),
        evento_id: 0,
        result_0 : parseInt(localStorage.getItem("sueldoAnual")),
        result_1: 0,
        result_2: 0,
        rut: localStorage.getItem("rut")
    }

    function cambiarme() {
        console.log("Enviando datos para traspaso a AFP Modelo");
        body_eventos.evento_id = 1;
        axios
            .post('Eventos', body_eventos, { headers: headers })
            .then((response) => {
                let data = response.data;

                if (response.status === 200) {
                    window.location.href =
                        "https://www.afpmodelo.cl/AFP/Cambiate-a-modelo/Formulario-De-Traspaso-Paso1.aspx?utm_source=AumentaTuSueldo&utm_medium=referal&utm_campaign=AumentaTuSueldo&utm_content=BotonAccion";
                } else if (!data.resultado) {
                    console.log(data);
                }
            })
            .catch(e => {
                console.log(e);
            });
        
    }

    function contactarme() {
        console.log("Enviando datos para solicitud con ejecutivo");

        body_eventos.evento_id = 2;
        axios
            .post('Eventos', body_eventos, { headers: headers })
            .then((response) => {
                let data = response.data;

                if (response.status === 200) {
                    window.location.href = "/solicitud";
                } else if (!data.resultado) {
                    console.log(data);
                }
            })
            .catch(e => {
                console.log(e);
            });
    }

    const rentabilidad_texto = "0,77% de comisión mensual, una de las más bajas del mercado.";
    const comision_texto = "Servicios digitales que hacen simple y rápidos tus trámites.";
    const atencion_texto = "Chatea con nuestra asistente virtual 24/7 y obtén asesoría cuando lo necesites.";
    const dudas_texto =
        "Nuestros ejecutivos pueden asesorarte en línea o vía telefónica. Queremos ayudarte a resolver todas tus inquietudes o darte todas las opciones para tu traspaso.";

    // eslint-disable-next-line
    useEffect(() => {
        let sueldoAnual = localStorage.getItem("sueldoAnual");
        let texto = "aumentar";
        if (sueldoAnual < 0) {
            texto = "disminuir";
            sueldoAnual = sueldoAnual * -1;
        }
        setText(texto);
        setSueldo(sueldoAnual);
    });

    return (
        <div>
            <Helmet>
                <title>{`Aumenta tu Sueldo en: ${sueldoFormateador(localStorage.getItem("sueldoAnual"))} | Esto Aumenta tu Sueldo al Año | AFP Modelo`}</title>
                <meta name="description" content="Aumentar mi sueldo líquido, pagando una menor comisión en AFP. Simula tu aumento de sueldo al cambiarte a AFP Modelo." />
                <meta name="robots" content="noindex, follow" />
            </Helmet>
            <div className="resultado">
                <section>
                    <div className="desktop">
                        <div className="row w-100 container-padre aumentar">
                            <div className="txtDesktop parrafo1">
                                <div className="container-title">
                                    <h5>Tu sueldo líquido podría {text}</h5>
                                    <h1 className="green">
                                        {sueldoFormateador(sueldo)+ " "}
                                        <span> al año.</span>
                                    </h1>
                                    <p>
                                        En AFP Modelo queremos ayudarte a tener una mejor pensión,
                                        para ello también nos encargamos de ofrecerte una menor
                                        comisión por nuestros servicios. Lo que hace que tu sueldo
                                        líquido aumente todos los meses. Puedes elegir pagar una
                                        menor comisión, puedes elegir ser parte de Modelo.
                                    </p>
                                    <button
                                        type="button"
                                        className="btn btn-lg btn-block"
                                        onClick={cambiarme}
                                        id="traspaso"
                                    >
                                        Quiero cambiarme a Modelo
                                    </button>
                                </div>
                            </div>
                            <img src={aumentarSueldo} alt="Aumenta tu sueldo" />
                        </div>
                        <div className="row w-100 container-padre flecha">
                            <img src={flecha} alt="down" />
                        </div>
                        <div className="row w-100 container-padre">
                            <div className="textosFloating">
                                <p>Aumenta tu sueldo.</p>
                                <p>Ahorrando en comisión.</p>
                                <p>Haz tu traspaso 100% online.</p>
                            </div>
                        </div>
                        <div className="row w-100 container-padre dudas">
                            <img src={mujerSAC} alt="Dudas" />
                            <div className="txtDesktop parrafo2">
                                <div className="container-title">
                                    <h5>¿Aún tienes dudas?</h5>
                                    <p>{dudas_texto}</p>
                                    <button
                                        type="button"
                                        className="btn blue btn-lg btn-block"
                                        onClick={contactarme}
                                        id="ejecutivo"
                                    >
                                        Quiero que me contacten
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="row w-100 container-padre ofrecemos">
                            <div className="col-12 d-flex justify-content-center mt-5">
                                <h5>¿Qué te ofrecemos?</h5>
                            </div>
                            <div className="bloque desktop">
                                <div className="card">
                                    <div className="card-header beneficio">
                                        <img
                                            className="icon rentabilidad"
                                            src={rentabilidad}
                                            alt="rentabilidad"
                                        />
                                    </div>
                                    <div className="card-body">
                                        <p>{rentabilidad_texto}</p>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header beneficio">
                                        <img
                                            className="icon comisiones"
                                            src={comisiones}
                                            alt="comisiones"
                                        />
                                    </div>
                                    <div className="card-body">
                                        <p>{comision_texto}</p>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header beneficio">
                                        <img
                                            className="icon sac"
                                            src={sac}
                                            alt="atención al cliente"
                                        />
                                    </div>
                                    <div className="card-body">
                                        <p>{atencion_texto}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mobile">
                        <div className="row w-100 justify-content-center d-block">
                            <div className="col-12 d-flex justify-content-center pt-4">
                                <div className="container-title">
                                    <p>TU SUELDO AUMENTARÍA</p>
                                    <div className="monto">
                                        <h1 className="green">
                                            {sueldoFormateador(sueldo)}
                                            {" "}
                                            <span>al año.</span>
                                        </h1>
                                    </div>
                                    <div className="sueldo">
                                        <div className="row">
                                            <div className="listado">
                                                <div className="beneficio">
                                                    <img
                                                        className="icon check"
                                                        src={check}
                                                        alt="check"
                                                    />
                                                    <p>Aumenta tu sueldo.</p>
                                                </div>
                                                <div className="beneficio">
                                                    <img
                                                        className="icon check"
                                                        src={check}
                                                        alt="check"
                                                    />
                                                    <p>Ahorrando en comisión.</p>
                                                </div>
                                                <div className="beneficio">
                                                    <img
                                                        className="icon check"
                                                        src={check}
                                                        alt="check"
                                                    />
                                                    <p>Haz tu traspaso 100% online.</p>
                                                </div>
                                                <button
                                                    type="button"
                                                    className="btn btn-lg btn-block"
                                                    onClick={cambiarme}
                                                >
                                                    Cambiarse a Modelo
                                                </button>
                                            </div>
                                            <img className="mujer" src={mujer} alt="mujer" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="dudas">
                                            <h5>¿Aún tienes dudas?</h5>
                                            <p>{dudas_texto}</p>
                                            <button
                                                type="button"
                                                className="btn blue btn-lg btn-block"
                                                onClick={contactarme}
                                            >
                                                Solicitar ejecutivo
                                            </button>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="dudas ofrecemos">
                                            <h5>¿Que te ofrecemos?</h5>
                                            <div className="listado">
                                                <div className="beneficio">
                                                    <img
                                                        className="icon rentabilidad"
                                                        src={rentabilidad}
                                                        alt="rentabilidad"
                                                    />
                                                    <p>{rentabilidad_texto}</p>
                                                </div>
                                                <div className="beneficio">
                                                    <img
                                                        className="icon comisiones"
                                                        src={comisiones}
                                                        alt="comisiones"
                                                    />
                                                    <p>{comision_texto}</p>
                                                </div>
                                                <div className="beneficio">
                                                    <img
                                                        className="icon sac"
                                                        src={sac}
                                                        alt="servicio al cliente"
                                                    />
                                                    <p>{atencion_texto}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}
