﻿import { useEffect } from "react";

const TagManager = () => {
    useEffect(() => {
        const scriptTagManager = document.createElement("script");
        scriptTagManager.id = "GoogleTagManagerjs";
        scriptTagManager.type = "text/javascript";
        scriptTagManager.async = false;
        scriptTagManager.innerHTML = "(​function​(w,d,s,l,i){w[l]=w[l]||[];w[l].push({​'gtm.start'​:new​​Date​().getTime(),event:​'gtm.js'​});​var​f=d.getElementsByTagName(s)[​0​],j=d.createElement(s),dl=l!=​'dataLayer'​?​'&l='​+l:​''​;j.async=​true​;j.src='https://www.googletagmanager.com/gtm.js?id='​+i+dl;f.parentNode.insertBefore(j,f);})(​window​,​document​,​'script'​,​'dataLayer'​,​'​GTM-PRRQB97​'​);​";

        const noScriptBody = document.createElement("script");
        noScriptBody.id = "noScriptGTM";
        noScriptBody.type = "Metadata/phrasing/flow";
        noScriptBody.async = false;
        noScriptBody.innerHTML = '(<​iframesrc=​"https://www.googletagmanager.com/ns.html?id=GTM-PRRQB97​"height=​"0"​ width=​"0"style=​"display:none;visibility:hidden"​></​iframe​>);';
        document.head.appendChild(scriptTagManager);
        document.body.appendChild(noScriptBody);
    }, []);

    return (null);
};

export default TagManager;
