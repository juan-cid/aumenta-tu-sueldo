import React from "react";
import logo from "../assets/img/logo.png";

const Header = () => (
    <nav className="navbar nav-fill w-100 navbar-dark bg-dark">
        <a className="navbar-brand mx-auto" href="https://www.afpmodelo.cl/AFP/Home.aspx">
            <img src={logo} alt="logo-afp-modelo" />
        </a>
    </nav>
);

export default Header;
