﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using monolith.Interfaces;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace monolith.Models
{
    public class ProspectosRepository : IProspectosRepository
    {
        private readonly IConfiguration _cfg;
        private string connectionString;

        public ProspectosRepository(IConfiguration cfg)
        {
            _cfg = cfg;
            this.connectionString = cfg.GetConnectionString("connectionString");
        }
      
        public bool put(Prospectos prospecto)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery =
                    "insert into [ATS_Prospectos_Testv1] (nombre,rut,email,sueldo,afp_origen,cod_Afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario,Resultado) values ";
                sQuery += "('" + prospecto.Nombre + "','" + prospecto.Rut + "','" + prospecto.Email + "','" +
                          prospecto.Sueldo + "','" + prospecto.Afp_Origen + "','" + prospecto.Cod_Afp_Origen + "','" +
                          prospecto.Fono_Movil + "','" + prospecto.Fec_Ing_reg + "','" +
                          prospecto.Canal + "','" + prospecto.Horario + "','" + prospecto.Resultado + "')";

                DynamicParameters parameters = new DynamicParameters();
                connection.Open();
                int resultado = 0;
                try
                {
                    resultado = SqlMapper.Execute(connection, sQuery);
                    if (resultado > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }


        public List<Prospectos> GetAllxFecha(string fecha)
        {
            using (var connection = new SqlConnection(connectionString))
            {
               
                string sQuery = $"select rut,nombre,email,fono_movil,afp_origen,cod_afp_origen,CONVERT(VARCHAR,CAST(SUBSTRING(fec_ing_reg,0,9) as DATE),5) as fec_ing_reg,canal,horario,sueldo,resultado from dbo.[ATS_Prospectos_Testv1] where fec_ing_reg like '{fecha}%' ";

                List<Prospectos> ListaProspectos = new List<Prospectos>();
                ListaProspectos = connection.Query<Prospectos>(sQuery).ToList();
                return ListaProspectos;
            }
        }

        public Prospectos ObtenerPorId(int id)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery = $"select nombre,rut,email,sueldo,afp_origen,cod_afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario,Id,Resultado from [ATS_Prospectos_Testv1] where id={id}";
                var  prop = connection.Query<Prospectos>(sQuery).FirstOrDefault();
                return prop;
            }
        }
    }
}