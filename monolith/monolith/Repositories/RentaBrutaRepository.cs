﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Extensions.Configuration;
using monolith.Interfaces;

namespace monolith.Models
{
    public class RentaBrutaRepository : IRentaBrutaRepository
    {
        private readonly IAtsRepository atsRepository;
        private readonly IConfiguration _configuration;

        public RentaBrutaRepository(IAtsRepository atsRepository, IConfiguration configuration)
        {
            this.atsRepository = atsRepository;
            _configuration = configuration;
        }
        
        
        public async Task<ResultadoAts> ObtenerRentaBruta(double RentaLiquida, int Afp)
        {
             //0=Capital
            //1=Cuprum
            //2=HAbitat
            //3=Planvital
            //4=Provida
            //5=Modelo
            //6=Uno
            ResultadoAts resultadoAts = new ResultadoAts();
            IndicadoresParAFP IndicadoresAFPCliente = atsRepository.GetComisionAfp(Afp);
            IndicadoresParAFP IndicadoresAFPModelo = atsRepository.GetComisionAfp(5);
            RentasTopes RentasTopes = atsRepository.GetSueldoTopeImponibles();
            IndicadoresPrev RangoISC = atsRepository.GetbySueldo(RentaLiquida);
            IndicadoresPrev RangoISCSup = atsRepository.ObtenerPorNivel(RangoISC.Nivel + 1,RangoISC.Mes,RangoISC.Año)??RangoISC;

            
            
            
            //de los indicadores saco el sueldo tope imponible

            var maxUfImponible = double.Parse(_configuration.GetSection("TopesAnuales")["SueldoImponibleMaxUF"],
                CultureInfo.InvariantCulture);
            
          
            
            var MAXIMO_IMPONIBLE = (double)IndicadoresAFPCliente.UfMes * maxUfImponible;

            var COMISION_AFP_MAS_AFPCLIENTE = 10 + IndicadoresAFPCliente.ComisionAfp;

            var COMISION_AFP_MAS_AFPMODELO = 10 + IndicadoresAFPModelo.ComisionAfp;
            
            var preRenta = (RentaLiquida - (double) RangoISC.CantidadRebajar) /
                           (1.0 - (double) RangoISC.Impuesto);

           

            var renta = Math.Round(preRenta, 2);

            var preRentaImponible = (renta - (double) RangoISC.CantidadRebajar) /
                                    (1.0 - (double) RangoISC.Impuesto);

            var sueldoBruto = preRentaImponible / (1 - ((COMISION_AFP_MAS_AFPCLIENTE / 100) + 0.07));


            var sueldoImponible = Math.Min(sueldoBruto, MAXIMO_IMPONIBLE);
            

            var SegCesantia = Math.Min((sueldoImponible * 0.006), (double) RentasTopes.SegCe);

            var Salud = sueldoImponible * 0.07;

            var aporteAfp = sueldoImponible * ((COMISION_AFP_MAS_AFPCLIENTE / 100));

            
            
            
            
            
            
            
            
            
            
            
            
            Double SueldoTributario = sueldoImponible - Salud - aporteAfp - SegCesantia;
            
            var RangoIscSupImpuest = (1 - RangoISCSup.Impuesto);
            var RangoIscSupCantidadRebajar = (SueldoTributario - RangoISCSup.CantidadRebajar);
            if (RangoIscSupImpuest == 0)
            {
                RangoIscSupImpuest = (1 - RangoISC.Impuesto);
            }

            if (RangoIscSupCantidadRebajar == 0)
            {
                RangoIscSupCantidadRebajar = (SueldoTributario - RangoISC.CantidadRebajar);
            }

            var SueldoTributario2 = (double)RangoIscSupImpuest / (double) RangoIscSupCantidadRebajar;
            

            Double RentaTopeAFP = RentasTopes.Afp;
            Double RentaTopeSegCe = RentasTopes.SegCe;

            double SueldoBrutoAFPActual = 0;
            double ComisionAFpActual = 0;
            double ComisionAFpModelo = 0;
            double DiferenciaComision = 0;

          
                if (SueldoTributario <= RentaTopeAFP)
                {

                    SueldoBrutoAFPActual = (SueldoTributario * SueldoTributario) / (SueldoTributario - ((SueldoTributario * 0.17) + (SueldoTributario * 0.006) + (SueldoTributario * IndicadoresAFPCliente.ComisionAfp / 100)));
                    ComisionAFpActual = SueldoBrutoAFPActual * (IndicadoresAFPCliente.ComisionAfp / 100);
                    ComisionAFpModelo = SueldoBrutoAFPActual * (IndicadoresAFPModelo.ComisionAfp / 100);
                    DiferenciaComision = ComisionAFpActual - ComisionAFpModelo;

                }
                else
                {
                    ComisionAFpActual = RentaTopeAFP * (IndicadoresAFPCliente.ComisionAfp / 100);
                    ComisionAFpModelo = RentaTopeAFP * (IndicadoresAFPModelo.ComisionAfp / 100);
                    DiferenciaComision = ComisionAFpActual - ComisionAFpModelo;

                }
         

            resultadoAts.SueldoAnual = Math.Round(DiferenciaComision) * 12;
            
            return resultadoAts;
        }
    }
}