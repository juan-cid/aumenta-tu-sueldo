﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using monolith.Interfaces;


namespace monolith.Models
{
    public class SimuladorRepository : ISimuladorRepository
    {
        private readonly IConfiguration cfg;
        private string connectionString;

        public SimuladorRepository(IConfiguration cfg)
        {
            this.cfg = cfg;
            connectionString = cfg.GetConnectionString("connectionString");
        }

        public async Task<bool> GuardarEnvioCorreo()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var d = DateTime.Now;
                var sql = $"insert into dbo.[EnvioSimulacionCorreo](FechaEnvio,Enviado) values ('{d}','{true}')";
                connection.Open();
                try
                {
                    
                    int resultado = 0;
                    resultado = SqlMapper.Execute(connection, sql);
                    if (resultado > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }

            return false;
        }

        public EnvioSimulacionCorreo ObtenerPorFecha(DateTime dia)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();
            var desde = dia.Date;
            var hasta = desde.AddHours(23).AddMinutes(59);
            string sQuery = $"select id,FechaEnvio,Enviado from [EnvioSimulacionCorreo] where FechaEnvio between '{desde}' and '{hasta}'";
            var  prop = connection.Query<EnvioSimulacionCorreo>(sQuery).FirstOrDefault();
            return prop;
        }

        public bool put(Simulador simulador)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery = "insert into [ATS_Simulacion_Test] (nombre,rut,correo,telefono_movil,sueldo,cod_afp_actual,fecha_simulacion,resultado) values ";
                sQuery += "('" + simulador.Nombre + "','" + simulador.Rut + "','" + simulador.Correo + "','" + simulador.Telefono_movil + "','" + simulador.Sueldo + "','" + simulador.Cod_Afp_Actual + "','" + simulador.fecha_simulacion.ToString("yyyy-MM-dd HH:mm:ss") + "','" + simulador.resultado +  "')";

                DynamicParameters parameters = new DynamicParameters();
                connection.Open();
                int resultado = 0;
                try
                {
                    resultado = SqlMapper.Execute(connection, sQuery);
                    if (resultado > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }




            }
            return false;
        }

    }
}
