﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using monolith.Interfaces;

namespace monolith.Models
{
  
    public class AfpsRepository : IAfpsRepository
    {
        private readonly IConfiguration cfg;
        private string connectionString;

        public AfpsRepository(IConfiguration cfg)
        {
            this.cfg = cfg;
            this.connectionString = cfg.GetConnectionString("connectionString");
        }

        public async Task<Afps> GetbyId(int id)
        {
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                string sQuery = "select id,Nombre,CodigoSIAFP from [dbo].[Afps] where id  = " + id;
                dbConnection.Open();
                return dbConnection.Query<Afps>(sQuery).FirstOrDefault();
            }
        }


    }
}
