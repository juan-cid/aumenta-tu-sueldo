﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using monolith.Interfaces;

namespace monolith.Models
{
   
    public class EventosRepository : IEventosRepository
    {
        private string connectionString;

        private readonly ILogger _logger;

        private readonly IConfiguration _cfg;

        public EventosRepository(IConfiguration cfg, ILogger<EventosRepository>evtLogger)
        {
            _logger = evtLogger;
            this.connectionString = cfg.GetConnectionString("connectionString");
            _cfg = cfg;
        }


        public IDbConnection Connection
        {
            get { return new SqlConnection(connectionString); }
        }

        public bool put(Eventos evento)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery ="insert into  [Calculadoras_eventos] (Session_id,Evento_id,result_0,result_1,result_2,Rut,HoraEvento) values ";

                sQuery += "('" + evento.Sesion_id + "','" + evento.Evento_id + "','" + evento.Result_0 + "','" +
                          evento.Result_1 + "','" + evento.Result_2 + "','"+ evento.Rut+"','"+ DateTime.UtcNow+"')";

                DynamicParameters parameters = new DynamicParameters();
                connection.Open();
                int resultado = 0;
                try
                {
                    resultado = SqlMapper.Execute(connection, sQuery);
                    if (resultado > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogCritical($"{ex.Message} {ex.InnerException?.Message}");
                    return false;
                }
            }

            return false;
        }
    }
}