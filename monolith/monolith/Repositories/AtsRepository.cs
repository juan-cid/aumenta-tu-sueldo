﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Validations;
using monolith.Interfaces;

namespace monolith.Models
{
    public class AtsRepository : IAtsRepository
    {
        private readonly IConfiguration cfg;
         

        private string connectionString;
        

        public AtsRepository(IConfiguration cfg)
        {
            this.cfg = cfg;
            
            connectionString = cfg.GetConnectionString("connectionString" );
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public IndicadoresPrev GetbySueldo(double sueldo)
        {
            using (IDbConnection dbConnection = Connection)
            {
                
                string sQuery = $"select id,Impuesto,CantidadRebajar,rentaliquidadesde,rentaliquidahasta,mes,año,nivel from [IndicadoresISC] where RentaliquidaHasta  >= " + sueldo + " and RentaLiquidaDesde <= " + sueldo;
                dbConnection.Open();
                Console.WriteLine(dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault());
                return dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault();
            }
        }


        public IndicadoresPrev GetbyId(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "select id,Impuesto,CantidadRebajar,rentaliquidadesde,rentaliquidahasta,mes,año,nivel from [IndicadoresISC] where id  = " + id;
                dbConnection.Open();
                return dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault();
            }
        }
        
        public IndicadoresPrev ObtenerPorNivel(int nivel,int mes,int año)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery =
                    $"select id,Impuesto,CantidadRebajar,rentaliquidadesde,rentaliquidahasta,mes,año,nivel from [IndicadoresISC] where nivel={nivel} and mes={mes} and año={año}"; 
                dbConnection.Open();
                return dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault();
            }
        }

        public Parametros leerParametros(string parametro)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery =
                    $"select parametro,Valor from [Parametros] where parametro='{parametro}' "; 
                dbConnection.Open();
                return dbConnection.Query<Parametros>(sQuery).FirstOrDefault();
            }
        }

        public async Task<ComisionesAFP> ObtenerComisiones()
        {
            
                //0=Capital
                //1=Cuprum
                //2=HAbitat
                //3=Planvital
                //4=Provida
                //5=Modelo
                //6=Uno
                var meses = new Dictionary<int, string>
                {
                    {1, "Enero"},
                    {2,"Febrero"},
                    {3,"Marzo"},
                    {4,"Abril"},
                    {5,"Mayo"},
                    {6,"Junio"},
                    {7,"Julio"},
                    {8,"Agosto"},
                    {9,"Septiembre"},
                    {10,"Octubre"},
                    {11,"Noviembre"},
                    {12,"Diciembre"}
                };

                using (IDbConnection dbConnection = Connection)
                {
                    string sQuery = $"select top (7) * from [IndicadoresPrev] order by Año desc";
                    dbConnection.Open();
                    try
                    {
                        var ind = dbConnection.Query<IndicadorComision>(sQuery).ToList();
                        return new ComisionesAFP
                        {
                            anio = DateTime.Now.Year,
                            mes = meses.FirstOrDefault(e => e.Key == DateTime.Now.Month).Value,
                            capital = ind.FirstOrDefault(t => t.Afp_Id ==0)?.ComisionAfp.ToString(),
                            cuprum = ind.FirstOrDefault(t => t.Afp_Id ==1)?.ComisionAfp.ToString(),
                            habitat = ind.FirstOrDefault(t => t.Afp_Id ==2)?.ComisionAfp.ToString(),
                            planvital = ind.FirstOrDefault(t => t.Afp_Id ==3)?.ComisionAfp.ToString(),
                            provida = ind.FirstOrDefault(t => t.Afp_Id ==4)?.ComisionAfp.ToString(),
                            modelo = ind.FirstOrDefault(t => t.Afp_Id ==5)?.ComisionAfp.ToString(),
                            uno = ind.FirstOrDefault(t => t.Afp_Id ==6)?.ComisionAfp.ToString()
                        };
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                   
                }

        }

        public IndicadoresParAFP GetComisionAfp(int afp)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = $"select Afp,TasaAfp,ComisionAfp,Afp_Id,UfMes,UtmMes,Mes,Año from [IndicadoresPrev] where Afp_id  = @Afp";
                dbConnection.Open();
                return dbConnection.Query<IndicadoresParAFP>(sQuery, new { Afp = afp }).FirstOrDefault();
            }
        }
        public RentasTopes GetSueldoTopeImponibles()
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = $"select Afp,SegCe from [SueldosTope]";
                dbConnection.Open();
                return dbConnection.Query<RentasTopes>(sQuery).FirstOrDefault();
            }
        }
        
        
    
        
    }
}
