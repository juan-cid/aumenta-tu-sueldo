﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using monolith.Interfaces;

namespace monolith.Models
{
    public class BlobService : IBlobService
    {
        private BlobServiceClient _blobServiceClient;
        private BlobContainerClient _blobContainerClient;
        private readonly IConfiguration _cfg;

        public BlobService(IConfiguration cfg)
        {
            _cfg = cfg;
            _blobServiceClient = new BlobServiceClient(cfg.GetSection("BlobStorage:ApiKey").Value);
            _blobContainerClient = new BlobContainerClient(cfg.GetSection("BlobStorage:ApiKey").Value,
                cfg.GetSection("BlobStorage:Container").Value);
        }
        
        public async Task<string> SubirArchivo(Stream archivoStream,string nombreArchivo)
        {
            nombreArchivo =  nombreArchivo.Replace("/", "-");
            BlobClient blobClient = _blobContainerClient.GetBlobClient(nombreArchivo);
            
           
            
                var resultado = await blobClient.UploadAsync(archivoStream, true);
                
                return resultado.Value.VersionId;
            

            
        }
    }
}