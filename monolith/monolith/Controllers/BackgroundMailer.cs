﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using monolith.Interfaces;
using monolith.Models;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace monolith.Controllers
{
    public class BackgroundMailer : BackgroundService
    {
        private readonly IProspectosRepository _prospectos;
        private readonly ISendgridServiceRepositorio _mailer;
        private readonly IAtsRepository _atsRepository;
        private readonly ISimuladorRepository _simulador;
        private readonly IConfiguration _configuration;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IBlobService _blobService;

        public BackgroundMailer(IProspectosRepository prospectos, ISendgridServiceRepositorio mailer, IAtsRepository atsRepository, ISimuladorRepository simulador, IConfiguration configuration, IBlobService blobService)
        {
            _prospectos = prospectos;
            _mailer = mailer;
            _atsRepository = atsRepository;
            _simulador = simulador;
            _configuration = configuration;
            _blobService = blobService;
        }

        public async Task<List<Prospectos>> GetProspectosDia()
        {
            var prospectos = _prospectos.GetAllxFecha(String.Format("{0:yyyyMMdd}", DateTime.Now.AddDays(-1)));
            // prospectos.ForEach(e =>
            // {
            //     DateTime.TryParse(e.Fec_Ing_reg.Substring(0, 8), out var diak);
            //     e.Fec_Ing_reg = diak.ToShortDateString();
            // });
            return prospectos ?? new List<Prospectos>();
        }

        public async Task EnviarCorreoProspectos()
        {
            var pros = await GetProspectosDia();
            var destinatarios = _atsRepository.leerParametros("CORREOS_SIMULACION")?.Valor;
            destinatarios ??= "asoto@afpmodelo.cl";

            if (pros.Any())
            {
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Reporte Excel");
                    worksheet.Cells["A1"].Value = "R.U.T";
                    worksheet.Cells["B1"].Value = "Nombre";
                    worksheet.Cells["C1"].Value = "Email";
                    worksheet.Cells["D1"].Value = "Fono Movil";
                    worksheet.Cells["E1"].Value = "AFP Origen";
                    worksheet.Cells["F1"].Value = "Codigo AFP Origen";
                    worksheet.Cells["G1"].Value = "Fecha Ingreso Registro";
                    worksheet.Cells["H1"].Value = "Canal";
                    worksheet.Cells["I1"].Value = "Horario";
                    worksheet.Cells["J1"].Value = "Sueldo";
                    worksheet.Cells["K1"].Value = "Resultado";
                    worksheet.Cells["A2"].LoadFromCollection(pros as IEnumerable<Prospectos>);
                    worksheet.Cells[1, 1, 10, 10].AutoFitColumns();

                    var archivo = new FileContentResult(package.GetAsByteArray(), XlsxContentType);
                    archivo.FileDownloadName = $"reporte_simulaciones_{DateTime.Now.AddDays(-1).ToShortDateString().Replace("/","-")}.xlsx";
                
                    Stream streamFile = new MemoryStream(archivo.FileContents);

                    var resultadoSubida = await _blobService.SubirArchivo(streamFile, archivo.FileDownloadName);
                    
                    var resultado = await _mailer.EnviarCorreoText("noreply@afpmodelo.cl", "Servicio Mailer ATS",
                        destinatarios,
                        "Servios Mailer",
                        "Reporte Simulaciones",
                        $"Se ha generado el reporte de simulaciones del día {DateTime.Now.ToShortDateString()} el archivo se llama: {archivo.FileDownloadName}");

                    if (resultado == "Accepted")
                    {
                     var mandado =  await  _simulador.GuardarEnvioCorreo();
                    }
                
                }
            }
           
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var hayRegistros = _simulador.ObtenerPorFecha(DateTime.UtcNow);
                    if (hayRegistros == null)
                    {
                        await EnviarCorreoProspectos();
                        await Task.Delay(TimeSpan.FromHours(6), stoppingToken);
                    }
                    
                    // await EnviarCorreoProspectos();
                    // await Task.Delay(TimeSpan.FromHours(6), stoppingToken);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    await Task.Delay(TimeSpan.FromHours(6), stoppingToken);
                    throw;
                }
               
                await Task.Delay(TimeSpan.FromHours(6), stoppingToken);
                
            }
        }

     
    }
}