﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using monolith.Interfaces;
using monolith.Models;

namespace monolith.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Backend_ATSController : Controller
    {
       
        private readonly IRentaBrutaRepository _rentaBrutaRepository;

        public Backend_ATSController(IRentaBrutaRepository rentaRepository)
        {
            _rentaBrutaRepository = rentaRepository;
        }
        //validar nombre,RUT,correo,sueldo y afp


        // POST api/<ProspectoController>
        [HttpPost]
        public Prospectos Post([FromBody] Prospectos value)
        {
            return value;
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Prospecto(Prospectos prospecto)
        {
            ResultadoAts resultadoAts = new ResultadoAts();

            if (prospecto == null)
            {
                return Ok(resultadoAts);

            }

            //validar

            if (rut.ValidaRut(prospecto.Rut)==false)
            {
                return Ok(resultadoAts);;
            }

            if (ValidaEmail.IsValidEmail(prospecto.Email) == false)
            {
                return Ok(resultadoAts);;
            }
            
            var resultado = await _rentaBrutaRepository.ObtenerRentaBruta(prospecto.Sueldo, int.Parse(prospecto.Cod_Afp_Origen));

            return Ok(resultado);


            //validar los campos

        }
    }
}
