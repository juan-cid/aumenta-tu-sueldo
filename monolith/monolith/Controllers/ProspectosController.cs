﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using monolith.Models;
using Syncfusion.XlsIO;
using System.IO;
using Syncfusion.Drawing;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using monolith.Interfaces;
using OfficeOpenXml;
using SendGrid;
using SendGrid.Helpers.Mail;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace monolith.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProspectosController : ControllerBase
    {

        private readonly IAfpsRepository _afps;
        private readonly IProspectosRepository _prospectosRepository;
        private readonly IAtsRepository _atsRepository;
        private readonly ISimuladorRepository _SimuladorRepository;
        private readonly ISendgridServiceRepositorio _Sendgrid;
        private readonly IRentaBrutaRepository _RentaBrutaRepository;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IHostingEnvironment _hostingEnvironment;


        public ProspectosController(IAfpsRepository afps, IProspectosRepository prospectosRepository, IAtsRepository atsRepository, ISimuladorRepository simuladorRepository, ISendgridServiceRepositorio sendgrid, IRentaBrutaRepository rentaBrutaRepository, IHostingEnvironment hostingEnvironment)
        {
            _afps = afps;
            _prospectosRepository = prospectosRepository;
            _atsRepository = atsRepository;
            _SimuladorRepository = simuladorRepository;
            _Sendgrid = sendgrid;
            _RentaBrutaRepository = rentaBrutaRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET api/<ProspectosController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id == 0) return BadRequest(new {mensaje = "debe indicar el id"});
            var res = _prospectosRepository.ObtenerPorId(id);
            if (res == null) return NotFound(new {mensaje = "no se encontro el registro"});
            return Ok(res);
        }

        // POST <ProspectosController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Prospectos value)
        {
            ResultadoAts resultadoAts = new ResultadoAts();

            if (value.Sueldo == 0)
            {
                return NotFound("Falta datos sueldo , obligatorio!!");
            }

            if (value.Nombre == null | value.Nombre == "")
            {
                return NotFound("Falta datos Nombre , obligatorio!!");
            }

            if (value.Cod_Afp_Origen == null | value.Cod_Afp_Origen == "")
            {
                return NotFound("Falta datos CodigoAfp , obligatorio!!");
            }

            if (value == null)
            {
                return NotFound("No hay datos de prospecto");
            }

            if (rut.ValidaRut(value.Rut) == false)
            {
                return NotFound("Error en validacion RUT");
            }

            if (ValidaEmail.IsValidEmail(value.Email) == false)
            {
                return NotFound("Error en validacion de correo ");
            }

            

         

            resultadoAts = await _RentaBrutaRepository.ObtenerRentaBruta(value.Sueldo, int.Parse(value.Cod_Afp_Origen));


            //llamar envio correo
            MetodosCorreo envioCorreo = new MetodosCorreo();

            string htmlcorreo = envioCorreo.GeneraHTML(resultadoAts.SueldoAnual.ToString(), value.Nombre);


            //envio
            var plainTextContent = "";
            var htmlContent = htmlcorreo;
            await _Sendgrid.EnviarCorreoHTML("noreply@aumentatusueldo.cl", "AFP Modelo", value.Email, value.Nombre,
                "Aumenta tu Sueldo", plainTextContent, htmlContent);
            

            string rutpaso = value.Rut.Replace("-", "").Replace(".", "");
            value.Rut = rutpaso.PadLeft(15, '0');
            value.Canal = "ATS";
            value.Fec_Ing_reg = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
            value.Fono_Movil = value.Fono_Movil;
            var apf =await  _afps.GetbyId(int.Parse(value.Cod_Afp_Origen));
            value.Afp_Origen = apf.Nombre;
            value.Cod_Afp_Origen = apf.CodigoSIAFP;
            value.Resultado = (resultadoAts.SueldoAnual);
            try
            {
                //guardar prospecto
                _prospectosRepository.put(value);
                try
                {
                    //guardar simulacion
                    
                    Simulador simulador = new Simulador();
                    simulador.Nombre = value.Nombre;
                    simulador.Rut = value.Rut;
                    simulador.Correo = value.Email;
                    simulador.Telefono_movil = value.Fono_Movil;
                    simulador.Sueldo = value.Sueldo;
                    simulador.Cod_Afp_Actual = value.Cod_Afp_Origen;
                    simulador.fecha_simulacion = DateTime.Now;
                    simulador.resultado = resultadoAts.SueldoAnual;

                    _SimuladorRepository.put(simulador);
                }
                catch(Exception e)
                {
                    //_logger.LogCritical($"{e.Message} \n {e.InnerException?.Message} \n {e.StackTrace} ");
                    return BadRequest(new {Message = $"{e.Message} \n {e.InnerException?.Message} \n {e.StackTrace} "});
                }
            }
            catch
            {
                return Ok(resultadoAts);
            }

            return Ok(resultadoAts);
        }

        // PUT api/<ProspectosController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ProspectosController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        [HttpGet]
        [Route("GetAllByDate")]
        public async Task<bool> GetAsync()
        {
            
            
            var prospectos = _prospectosRepository.GetAllxFecha(String.Format("{0:yyyyMMdd}", DateTime.Now));
            prospectos.ForEach(e =>
            {
                DateTime.TryParse(e.Fec_Ing_reg.Substring(0, 8), out var diak);
                e.Fec_Ing_reg = diak.ToShortDateString();
            });

            
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Reporte Excel");
                worksheet.Cells["A1"].Value = "R.U.T";
                worksheet.Cells["B1"].Value = "Nombre";
                worksheet.Cells["C1"].Value = "Email";
                worksheet.Cells["D1"].Value = "Fono Fijo";
                worksheet.Cells["E1"].Value = "Fono Movil";
                worksheet.Cells["F1"].Value = "AFP Origen";
                worksheet.Cells["G1"].Value = "Codigo AFP Origen";
                worksheet.Cells["H1"].Value = "Fecha Ingreso Registro";
                worksheet.Cells["I1"].Value = "Canal";
                worksheet.Cells["J1"].Value = "Horario";
                worksheet.Cells["K1"].Value = "Sueldo";
                worksheet.Cells["L1"].Value = "Resultado";
                worksheet.Cells["A2"].LoadFromCollection(prospectos as IEnumerable<Prospectos>);
                worksheet.Cells[1,1,10,10].AutoFitColumns();
                
                 var destinatarios = _atsRepository.leerParametros("CORREOS_SIMULACION")?.Valor;
                
                var archivo = File(package.GetAsByteArray(), XlsxContentType, "report.xlsx");
                await _Sendgrid.EnviarCorreoAttachment("noreply@afpmodelo.cl", "tendro", destinatarios, "rein",
                    "excel reporte", "lololo ahi va el archivo", string.Empty, archivo);
                
            }
            
            
            #region Oblivion
      
            //     //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            //
            //     //StorageCredentials cred = new StorageCredentials("Azure\\storagefileats", "fNWBNgTXkSUptfJ7oL0fJGTxz5wY/vyskfRg+phw2BrLVfgaBd6L001N22SNVquY6Yifdm+G4MQOir9JMP93YQ=="); 
            //     //CloudBlobContainer container = new CloudBlobContainer(new Uri("http://Azure\\storagefileats.blob.core.windows.net/storagefileats.file.core.windows.net  /"), cred); 
            //
         
            #endregion


            return true;
        }
        
      
        [Route("obtenercomisiones")]
        [HttpGet]
        public async Task<IActionResult> obtenercomisiones()
        {
            var data = await _atsRepository.ObtenerComisiones();
            if (data == null) return NotFound(new {mensaje = "no se encontraron registros!."});
            return Ok(data);
        }
        
        
    }
}