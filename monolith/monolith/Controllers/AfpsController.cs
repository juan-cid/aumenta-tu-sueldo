﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using monolith.Interfaces;
using monolith.Models;


namespace monolith.Controllers
{
    public class AfpsController : Controller
    {
        public readonly IAfpsRepository afpRepository;


        public AfpsController(IAfpsRepository afpRepository)
        {
            this.afpRepository = afpRepository;
        }


        //[HttpGet("{id}")]

        public async Task<Afps> Get(int id)
        {

            Afps miafp = new Afps();
            miafp = await afpRepository.GetbyId(id);
            return miafp;

        }

        }
    }
