﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using monolith.Interfaces;
using monolith.Models;
namespace monolith.Controllers
{
    public class crentaliqController : Controller
    {
        public readonly IRentaBrutaRepository _rentaRepository;

        public crentaliqController(IRentaBrutaRepository rentaRepository)
        {
            _rentaRepository = rentaRepository;
        }
        
        [HttpGet]
        [Route("rentaliquida/afp")]
        public async Task<ResultadoAts> Get(double RentaLiquida, int Afp)
        {

            var result = await _rentaRepository.ObtenerRentaBruta(RentaLiquida, Afp);
            return result;
        }

    }
}
