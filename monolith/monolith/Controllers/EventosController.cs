﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using monolith.Interfaces;
using monolith.Models;

namespace monolith.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class EventosController : Controller
    {
        public readonly IEventosRepository eventosRepository;

        public EventosController(IEventosRepository eventosRepository)
        {
            this.eventosRepository = eventosRepository;
        }

      
        // POST <ProspectosController>
        [HttpPost]
        public IActionResult Post([FromBody] Eventos value)
        {
            ResultadoEventos resultadoevento = new ResultadoEventos();
            
            bool resultado = eventosRepository.put(value);

            resultadoevento.resultado = resultado;
            return Ok(resultadoevento);


        }

    }
}
