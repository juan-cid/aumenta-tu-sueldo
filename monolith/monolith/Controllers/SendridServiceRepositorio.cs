﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using monolith.Interfaces;
using OfficeOpenXml.Drawing.Chart;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace monolith.Controllers
{
    public class SendgridServiceRepositorio : ISendgridServiceRepositorio
    {
        private readonly IServiceProvider services;

        public SendgridServiceRepositorio(IServiceProvider service)
        {
            services = service;
        }
        public async Task<string> EnviarCorreoText(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto, string texto)
        {
            var client = this.services.GetRequiredService<ISendGridClient>();
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(mail_remitente, remitente),
                Subject = asunto
            };
            msg.AddContent(MimeType.Text, texto);
            msg.AddTo(new EmailAddress(mail_receptor, nombre));
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            return  $"{response.StatusCode}";

        }

        public async Task<string> EnviarCorreoHTML(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto, string textoplano, string html)
        {
            var client = this.services.GetRequiredService<ISendGridClient>();
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(mail_remitente, remitente),
                Subject = asunto,
                PlainTextContent = textoplano,
                HtmlContent = html
            };
            msg.AddTo(new EmailAddress(mail_receptor, nombre));
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            return  $"{response.StatusCode}";
        }

        public async Task<string> EnviarCorreoAttachment(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto,
            string textoplano,string html,FileContentResult archivo)
        {
            
            var client = this.services.GetRequiredService<ISendGridClient>();
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(mail_remitente, remitente),
                Subject = asunto,
                PlainTextContent = textoplano,
                HtmlContent = html??string.Empty
            };
            if (mail_receptor.Contains(";"))
            {
                var correos = mail_receptor.Split(";").Skip(1).ToList();
                var listaEmails = new List<EmailAddress>();
                correos.ForEach(destinatario => listaEmails.Add(new EmailAddress(destinatario)));
                msg.AddCcs(listaEmails);
                msg.AddTo(new EmailAddress(mail_receptor.Split(";")[0], nombre));
            }
            else
            {
                msg.AddTo(new EmailAddress(mail_receptor, nombre));
            }
            
            
            Stream stream = new MemoryStream(archivo.FileContents);
            await msg.AddAttachmentAsync(archivo.FileDownloadName, stream, archivo.ContentType);
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            return $"{response.StatusCode}";

        }
    }
}
