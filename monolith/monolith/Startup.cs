using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using monolith.Controllers;
using monolith.Interfaces;
using monolith.Models;
using SendGrid.Extensions.DependencyInjection;
using Syncfusion.XlsIO.Parser.Biff_Records;

namespace monolith
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddCors(options => options.AddPolicy("CorsPolicy",
            //     builder => builder.AllowAnyMethod().AllowAnyHeader().AllowCredentials().Build()));

            services.AddCors();

            services.AddLogging(log => log.AddDebug());
            services.AddSendGrid(options => options.ApiKey = Configuration.GetSection("SendGrid")["SENDGRID_API_KEY"]);
            services.AddSingleton(Configuration);
            services.AddTransient<IBlobService, BlobService>();
            services.AddTransient<IAfpsRepository, AfpsRepository>();
            services.AddTransient<IRentaBrutaRepository,RentaBrutaRepository>();
            services.AddTransient<IAtsRepository,AtsRepository>();
            services.AddTransient<ISimuladorRepository, SimuladorRepository>();
            services.AddTransient<IProspectosRepository, ProspectosRepository>();
            services.AddTransient<IEventosRepository,EventosRepository>();
            services.AddTransient<ISendgridServiceRepositorio, SendgridServiceRepositorio>();
            services.AddControllersWithViews();
            //AddSwagger(services);
            services.AddHostedService<BackgroundMailer>();
            services.AddSwaggerGen(swagger =>
            {
                
                // To Enable authorization using Swagger (JWT)  
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()  
                {  
                    Name = "Authorization",  
                    Type = SecuritySchemeType.ApiKey,  
                    Scheme = "Bearer",  
                    BearerFormat = "JWT",  
                    In = ParameterLocation.Header,  
                    Description = "JWT header usando  esquema Bearer. \r\n\r\n Ingresa el 'Bearer' [espacio] y luego tu token en la caja de texto debajo.\r\n\r\nEjemplo: \"Bearer abcdef\"",  
                });  
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement  
                {  
                    {  
                        new OpenApiSecurityScheme  
                        {  
                            Reference = new OpenApiReference  
                            {  
                                Type = ReferenceType.SecurityScheme,  
                                Id = "Bearer"  
                            }  
                        },  
                        new string[] {}  
  
                    }  
                });  
            });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            // app.UseCors(option => option
            //     .AllowAnyOrigin()
            //     .AllowAnyMethod()
            //     .AllowAnyHeader());
            
            app.UseCors(opciones => opciones
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials()); // cualquier origen
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Aumenta Tu Sueldo API V1");
            });

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                /*
                 spa.Options.SourcePath = "ClientApp";
                
                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
                */
            });
        }


        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"Aumenta tu Sueldo {groupName}",
                    Version = groupName,
                    Description = "API Calculadora Aumenta Tu Sueldo",
                    Contact = new OpenApiContact
                    {
                        Name = "Aumenta Tu Sueldo",
                        Email = string.Empty,
                        Url = new System.Uri("https://aumentatusueldo.cl/"),
                    }
                });
            });
        }
    }
}
